#pragma once

#include <string>
#include <map>

#include <SFML/Graphics.hpp>



class FontLoader {

public:
    static bool loadFromFile(std::string fontName, std::string filename);
    static void load(std::string fontName, sf::Font* font);

    static void setDefault(std::string fontName);

    static sf::Font* getFont(std::string fontName);
    static sf::Font* getDefault();

protected:
    static std::map<std::string, sf::Font*> fonts;
    static std::string defaultFontName;

};
