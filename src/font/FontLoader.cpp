#include "FontLoader.hpp"

#include <iostream>



bool FontLoader::loadFromFile(std::string fontName, std::string filename) {
    sf::Font* font = new sf::Font();
    if (!font->loadFromFile(filename)) {
        std::cerr << "FontLoader::loadFromFile: couldn't load font from file '" << fontName << "'\n";
        delete font;
        return false;
    }
    load(fontName, font);
    return true;
}

void FontLoader::load(std::string fontName, sf::Font* font) {
    fonts.insert(std::pair<std::string, sf::Font*>(fontName, font));
    if (defaultFontName == "") {
        setDefault(fontName);
    }
}


void FontLoader::setDefault(std::string fontName) {
    defaultFontName = fontName;
}


sf::Font* FontLoader::getFont(std::string fontName) {
    std::map<std::string, sf::Font*>::iterator it = fonts.find(fontName);
    if (it == fonts.end()) {
        std::cerr << "FontLoader::getFont: undefined font '" << fontName << "', loading default\n";
        return getDefault();
    }
    return it->second;
}

sf::Font* FontLoader::getDefault() {
    std::map<std::string, sf::Font*>::iterator it = fonts.find(defaultFontName);
    if (it == fonts.end()) {
        std::cerr << "FontLoader::getFont: default font ('" << defaultFontName << "') is undefined\n";
        return nullptr;
    }
    return it->second;
}


std::map<std::string, sf::Font*> FontLoader::fonts;
std::string FontLoader::defaultFontName = "";
