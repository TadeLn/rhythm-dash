#include "Camera.hpp"


#include "util/Util.hpp"



void Camera::setShouldUpdateCamera() {
    this->updateCamera = true;
}

void Camera::clearShouldUpdateCamera() {
    this->updateCamera = false;
}

bool Camera::shouldUpdateCamera() {
    return this->updateCamera;
}


tu::Pair<float> Camera::getPosition() {
    return this->position;
}

float Camera::getZoom() {
    return this->zoom;
}

float Camera::getRotation() {
    return this->rotation;
}


void Camera::setPosition(tu::Pair<float> position) {
    setShouldUpdateCamera();
    this->position = position;
}

void Camera::setPosition(float x, float y) {
    this->setPosition(tu::Pair<float>(x, y));
}

void Camera::setZoom(float zoom) {
    if (zoom != this->zoom) {
        setShouldUpdateCamera();
        this->zoom = zoom;
        if (this->zoom > 50) {
            this->zoom = 50;
        }
        if (this->zoom < 0.001) {
            this->zoom = 0.001;
        }
    }
}

void Camera::setRotation(float rotation) {
    setShouldUpdateCamera();
    this->rotation = rotation;
}


void Camera::move(tu::Pair<float> offset) {
    this->setPosition(this->position + offset);
}

void Camera::move(float x, float y) {
    this->move(tu::Pair<float>(x, y));
}


Camera::Camera() {
    this->position = tu::Pair<float>(0, 0);
    this->zoom = 1;
    this->rotation = 0;
}

std::string Camera::toString() const {
    return Util::str("{",
        this->position,
        ", z: ", this->zoom,
        ", r: ", this->rotation,
        "}");
}
