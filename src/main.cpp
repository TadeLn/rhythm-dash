#include <csignal>

#include "Game.hpp"
#include "tutils/Log.hpp"


Game* game = nullptr;


void signal(int signum) {
    game->handleSignal(signum);
    exit(signum); 
}


int main(int argc, char** argv) {
    std::signal(SIGINT, signal); 

    std::vector<std::string> args;
    for (int i = 0; i < argc; i++) {
        args.push_back(std::string(argv[i]));
    }

    game = new Game();
    return game->start(args);
}