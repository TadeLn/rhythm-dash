#include "World.hpp"


#include "tutils/Log.hpp"
#include "tutils/Exception.hpp"
#include "tutils/Pair.hpp"
#include "tutils/json/Node.hpp"
#include "tutils/json/Object.hpp"
#include "tutils/json/Array.hpp"

#include "renderer/TextureManager.hpp"
#include "entity/Player.hpp"



Player& World::getPlayer() {
    return *this->player;
}

std::vector<std::string>& World::getPalette() {
    return this->palette;
}



void World::updatePalette() {
    paletteSprites.clear();
    for (int i = 0; i < palette.size(); i++) {
        paletteSprites.push_back(
            TextureManager::getInfo(std::string("tile/") + palette[i])
        );
    }
    
    SpriteInfo undefined = TextureManager::getInfo("undefined");
    while (paletteSprites.size() < 256) {
        paletteSprites.push_back(undefined);
    }
}



std::map<tu::Pair<int>, std::shared_ptr<Chunk>>& World::getChunks() {
    return this->chunks;
}



std::shared_ptr<Chunk> World::getChunk(tu::Pair<int> chunkPos) {
    auto a = this->chunks.find(chunkPos);
    if (a != this->chunks.end()) {
        return a->second;
    }
    return std::shared_ptr<Chunk>(nullptr);
}

std::shared_ptr<Chunk> World::getChunk(int chunkX, int chunkY) {
    return getChunk(tu::Pair(chunkX, chunkY));
}

void World::setChunk(tu::Pair<int> chunkPos, std::shared_ptr<Chunk> chunk) {
    this->chunks[chunkPos] = chunk;
}

void World::setChunk(int chunkX, int chunkY, std::shared_ptr<Chunk> chunk) {
    return setChunk(tu::Pair(chunkX, chunkY), chunk);
}


void World::spawn(std::shared_ptr<Entity> entity) {
    long uuid = entity->getUUID();

    if (entities.find(uuid) != entities.end()) {
        throw tu::Exception("Could not spawn entity: entity already exists", EXCTX);
    }

    if (entity->onSpawn(this)) {
        entities[uuid] = entity;
    }
}



TileID World::getTile(tu::Pair<int> position) {
    tu::Pair<int> chunkPos = tu::Pair(floor((float)position.x / CHUNK_SIZE), floor((float)position.y / CHUNK_SIZE));
    auto chunk = this->getChunk(chunkPos);

    if (chunk) {
        return chunk->getTile(position.x - chunkPos.x * CHUNK_SIZE, position.y - chunkPos.y * CHUNK_SIZE);
    } else {
        return 0;
    }
}

TileID World::getTile(int x, int y) {
    return getTile(tu::Pair(x, y));
}

std::map<long, std::shared_ptr<Entity>>& World::getEntities() {
    return this->entities;
}



void World::init() {
    updatePalette();

    std::shared_ptr<Player> player = std::make_shared<Player>();
    spawn(std::dynamic_pointer_cast<Entity>(player));

    tu::Log::log("Initialized world");
}

void World::reload() {
    throw tu::Exception("Reloading not avaliable yet", tu::Exception::METHOD_NOT_IMPLEMENTED, EXCTX);
}





bool World::loadJSON(std::shared_ptr<tu::json::Node> j) {
    using namespace tu::json;

    try {
        Object* root = j->cast<Object>();

        auto palette = root->getProperty("palette")->getOr<Node::array>();
        for (int i = 0; i < palette.size(); i++) {
            auto paletteEntry = palette[i]->getOr<std::string>();
            this->palette.push_back(paletteEntry);
        }

        auto chunks = root->getProperty("chunks")->getOr<Node::array>();
        for (int i = 0; i < chunks.size(); i++) {

            auto chunkData = chunks[i]->cast<Object>();

            auto chunkPosX = chunkData->getProperty("x")->getEx<int>();
            auto chunkPosY = chunkData->getProperty("y")->getEx<int>();

            auto tiles = chunkData->getProperty("tiles")->getEx<tu::json::Node::array>();

            if (tiles.size() < CHUNK_SIZE * CHUNK_SIZE) {
                throw tu::Exception(Util::str("chunks[", i, "].tiles is not long enough"), EXCTX);
            }
            
            std::shared_ptr<Chunk> chunk = std::make_shared<Chunk>();

            for (int j = 0; j < CHUNK_SIZE * CHUNK_SIZE; j++) {
                chunk->setTile(j, tiles[j]->getOr<TileID>(0));
            }

            this->setChunk(chunkPosX, chunkPosY, chunk);
        }

    } catch (tu::Exception& e) {
        throw tu::Exception(Util::str("Error while loading level ", this->filename, ": ", e.getMessage()), EXCTX);

    } catch (std::exception& e) {
        throw tu::Exception(Util::str("Error while loading level ", this->filename, ": ", e.what()), EXCTX);
    }
    
    return true;
}
