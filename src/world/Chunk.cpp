#include "Chunk.hpp"


#include "entity/Entity.hpp"



TileID Chunk::getTile(int index) {
    return tiles[index];
}

TileID Chunk::getTile(Byte x, Byte y) {
    return tiles[x + (y * CHUNK_SIZE)];
}

void Chunk::setTile(int index, TileID tile) {
    tiles[index] = tile;
}

void Chunk::setTile(Byte x, Byte y, TileID tile) {
    tiles[x + (y * CHUNK_SIZE)] = tile;
}


Entity* Chunk::getObject(long UUID) {
    for (Entity* obj : objects) {
        if (obj->getUUID() == UUID) {
            return obj;
        }
    }
    return nullptr;
}

void Chunk::addObject(Entity* obj) {
    objects.addObject(obj);
}



const EntitySet* Chunk::getObjects() const {
    return &this->objects;
}



Chunk Chunk::copy() {
    Chunk result;
    std::copy_n(this->tiles.begin(), this->tiles.size(), result.tiles.begin());
    result.objects = this->objects.copy();
    return result;
}

Chunk::Chunk(const Chunk& sourceChunk) {
    std::copy_n(sourceChunk.tiles.begin(), sourceChunk.tiles.size(), this->tiles.begin());
    this->objects = sourceChunk.getObjects()->copy();
}
