#pragma once


#include <memory>
#include <map>

#include "tutils/Pair.hpp"
#include "tutils/json/Deserializable.hpp"

class World;

#include "renderer/SpriteInfo.hpp"
#include "world/Chunk.hpp"

class Player;



class World : public tu::json::Deserializable {


public:
    Player& getPlayer();

    std::vector<std::string>& getPalette();


    std::map<tu::Pair<int>, std::shared_ptr<Chunk>>& getChunks();
    
    std::shared_ptr<Chunk> getChunk(tu::Pair<int> chunkPos);
    std::shared_ptr<Chunk> getChunk(int chunkX, int chunkY);

    TileID getTile(tu::Pair<int> position);
    TileID getTile(int x, int y);
    std::map<long, std::shared_ptr<Entity>>& getEntities();



    void setChunk(tu::Pair<int> chunkPos, std::shared_ptr<Chunk> chunk);
    void setChunk(int chunkX, int chunkY, std::shared_ptr<Chunk> chunk);

    void spawn(std::shared_ptr<Entity> entity);


    std::vector<SpriteInfo> paletteSprites;
    void updatePalette();


    // Called after the world has been loaded
    void init();

    // Called after the player died and restarted the level
    void reload();


    World() = default;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);


private:
    std::string filename;
    std::vector<std::string> palette;
    std::map<tu::Pair<int>, std::shared_ptr<Chunk>> chunks;
    std::map<long, std::shared_ptr<Entity>> entities;
    std::shared_ptr<Player> player;


};