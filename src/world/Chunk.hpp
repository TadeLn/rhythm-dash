#pragma once


#include "world/Tile.hpp"
#include "entity/EntitySet.hpp"
#include "util/Util.hpp"

class Entity;



#define CHUNK_SIZE 32


class Chunk {


public:

    TileID getTile(int index);
    TileID getTile(Byte x, Byte y);
    void setTile(int index, TileID tile);
    void setTile(Byte x, Byte y, TileID tile);

    Entity* getObject(long UUID);
    void addObject(Entity* obj);

    const EntitySet* getObjects() const;
    
    // Construct empty chunk
    Chunk() = default;

    // Make a full copy of a chunk
    Chunk copy();

    // Make a full copy of a chunk
    Chunk(const Chunk& sourceChunk);


private:
    std::array<TileID, CHUNK_SIZE * CHUNK_SIZE> tiles;
    EntitySet objects;


};
