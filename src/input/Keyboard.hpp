#pragma once


#include <map>
#include <SFML/Window.hpp>



class Keyboard {


public:
    static void update(sf::Event e);

    static bool get(int keyCode);


private:
    static std::map<int, bool> state;


};