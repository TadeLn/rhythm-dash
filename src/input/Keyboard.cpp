#include "Keyboard.hpp"

#include "tutils/Log.hpp"



void Keyboard::update(sf::Event e) {
    if (e.type == sf::Event::KeyPressed) {
        Keyboard::state[e.key.code] = true;
    } else if (e.type == sf::Event::KeyReleased) {
        Keyboard::state[e.key.code] = false;
    } else return;

    // tu::Log::log("Keyboard update: (", e.key.code, ") => ", Keyboard::state[e.key.code]);
}

bool Keyboard::get(int keyCode) {
    std::map<int, bool>::iterator it = Keyboard::state.find(keyCode);
    if (it != Keyboard::state.end()) {
        return it->second;
    } else {
        return 0;
    }
}

std::map<int, bool> Keyboard::state;