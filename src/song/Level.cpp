#include "Level.hpp"


#include "tutils/json/Object.hpp"



void Level::reload() {
    // [TODO]
    throw tu::Exception("[TODO]", EXCTX);
}


Level Level::loadFromFile(std::string filename) {
    Level result = Level();
    result.filename = filename;
    result.loadJSON(tu::json::Node::fromFile(filename));
    return result;
}

bool Level::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();
    
    root->getProperty("test")->load(this->test);
    root->getProperty("backgroundColor")->load(this->backgroundColor);
    this->world.loadJSON(root->getProperty("world"));

    return true;
}
