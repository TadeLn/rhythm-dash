#pragma once


#include <vector>

#include "tutils/Pair.hpp"
#include "tutils/json/Deserializable.hpp"

#include "song/SongMetadata.hpp"
#include "timing/TimingSection.hpp"
#include "world/World.hpp"



// This class stores data of the song's gameplay: timing, world, appearance, etc.
class Level : tu::json::Deserializable {


public:

    std::string filename;

    std::string test = "<no test string given>";
    unsigned int backgroundColor = 0;

    World world;
    std::vector<tu::BasicPair<int, TimingSection>> timing;
    

    void reload();

    static Level loadFromFile(std::string filename);
    bool loadJSON(std::shared_ptr<tu::json::Node> json);


};
