#pragma once


#include <string>
#include <vector>



// This class stores data displayed on the song selection menu
class SongMetadata {


public:

    class Link {

    public:
        std::string icon;
        std::string text;
        std::string url;
        
    };


    // Song folder location
    std::string location;

    // Song metadata
    std::string title;
    std::string artist;
    std::string album;
    std::string game;
    int year;

    // Links to the song / album / artist
    std::vector<Link> links;

    // Chart metadata
    std::string charter;
    std::string description;
    int songLength;
    int difficulty;

    // Song preview info
    std::string previewFilename;
    int previewStart;
    int previewLength;


};
