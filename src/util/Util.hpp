#pragma once


#include <string>
#include <sstream>

#include "tutils/String.hpp"



typedef unsigned char Byte;



namespace Util {


template<typename... Args>
std::string str(const Args& ...args) {
    return tu::String::str(args...);
}


};
