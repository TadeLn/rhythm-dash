#pragma once


#include <string>



class Debug {

public:
    static void add(std::string text);
    static void addLine(std::string line);
    static std::string getString();
    static void clearString();

    static bool enabled;
    static bool cameraDebug;

private:
    static std::string debugString;



};