#include "Debug.hpp"



void Debug::add(std::string text) {
    debugString.append(text);
}

void Debug::addLine(std::string line) {
    debugString.append(line + '\n');
}

std::string Debug::getString() {
    return debugString;
}

void Debug::clearString() {
    debugString = "";
}

bool Debug::enabled = true;
bool Debug::cameraDebug = false;

std::string Debug::debugString = "";
