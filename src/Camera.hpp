#pragma once


#include "tutils/Pair.hpp"
#include "tutils/Stringable.hpp"



class Camera : public tu::Stringable {


public:
    void setShouldUpdateCamera();
    void clearShouldUpdateCamera();
    bool shouldUpdateCamera();

    tu::Pair<float> getPosition();
    float getZoom();
    float getRotation();

    void setPosition(tu::Pair<float> position);
    void setPosition(float x, float y);
    void setZoom(float zoom);
    void setRotation(float rotation);

    void move(tu::Pair<float> offset);
    void move(float x, float y);

    Camera();

    std::string toString() const;

private:
    tu::Pair<float> position;
    float zoom;
    float rotation;
    bool updateCamera = false;


};
