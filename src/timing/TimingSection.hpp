#pragma once



class TimingSection {


public:
    // How many quarternotes in a minute
    int bpm;
    
    // How many beats in a measure
    int beats;
    
    // What part of a whole note is one beat
    // (4 => 1 beat = 1 quarter note)
    // (8 => 1 beat = 1 eighth note)
    int subdivisions;


    int getBeatTime(int measureIndex, int beatIndex);
    int getBeatTime(int beatIndex);



};