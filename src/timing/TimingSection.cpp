#include "TimingSection.hpp"



int TimingSection::getBeatTime(int measureIndex, int beatIndex) {
    return getBeatTime((measureIndex * this->beats) + beatIndex);
}

int TimingSection::getBeatTime(int beatIndex) {
    const int msPerQuarterNote = 60000 / bpm;
    const int msPerBeat = (msPerQuarterNote * 4) / beats;
    return beatIndex * msPerBeat;
}
