#pragma once


#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class Game;

#include "renderer/Renderer.hpp"
#include "world/World.hpp"
#include "song/Level.hpp"
#include "screen/Screen.hpp"



class Game {


public:
    Renderer* r;

    Level& getLevel();
    World& getWorld();

    int start(std::vector<std::string> args);

    // Force game stop
    void stop();

    bool isRunning();

    // Request game exit
    void exit();


    void handleKey(sf::Event e);
    void handleMouse(sf::Event e);
    void handleResize(sf::Event e);
    void handleSignal(int signum);

    Screen* currentScreen = nullptr;
    void openScreen(Screen* screen);
    void replaceScreen(Screen* screen);
    void closeScreen();

    Game() = default;


private:
    Level level;

    void loop(float deltaTime);
    bool running;

};
