#pragma once



#include <vector>

class Entity;


class EntitySet {


public:
    Entity* getObject(unsigned int index);
    void addObject(Entity* obj);
    void deleteObject(unsigned int index);
    void removeObject(unsigned int index); // does not delete the object; might lead to memory leaks
    int size();

    std::vector<Entity*>::iterator begin();
    std::vector<Entity*>::iterator end();

    std::vector<Entity*> getSortedVector(); // sorts objects by z index

    // Make a full copy
    EntitySet copy() const;

    // Make a new list, with the same object pointers
    EntitySet(EntitySet& set);

    // Make a new, empty object list
    EntitySet();


private:
    std::vector<Entity*> objects;


};
