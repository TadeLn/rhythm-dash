#include "SpriteEntity.hpp"


#include "renderer/TextureManager.hpp"



void SpriteEntity::draw(sf::RenderTarget* target) {
    sprite.setPosition(sf::Vector2f(
        pos.x - spriteInfo.origin.x,
        pos.y - spriteInfo.origin.y
    ));
    target->draw(this->sprite);
}

SpriteEntity::SpriteEntity() {
    sprite = sf::Sprite();
    sprite.setTexture(TextureManager::getTexture());
    this->updateSprite();
}

SpriteEntity::SpriteEntity(std::string spriteId) : SpriteEntity() {
    this->setSprite(spriteId);
}


void SpriteEntity::setSprite(std::string spriteId) {
    if (this->spriteId != spriteId) {
        this->spriteId = spriteId;
        this->updateSprite();
    }
}

void SpriteEntity::setSpriteAnimation(int spriteAnimation) {
    if (this->spriteAnimation != spriteAnimation) {
        this->spriteAnimation = spriteAnimation;
        this->updateSprite();
    }
}


void SpriteEntity::updateSprite() {
    spriteInfo = TextureManager::getInfo(spriteId, spriteAnimation);
    sprite.setTextureRect(spriteInfo.rect);
}
