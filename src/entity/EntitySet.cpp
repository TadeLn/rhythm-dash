#include "EntitySet.hpp"


#include <algorithm>

#include "entity/Entity.hpp"



Entity* EntitySet::getObject(unsigned int index) {
    if (index >= objects.size()) {
        return nullptr;
    }
    return objects[index];
}

void EntitySet::addObject(Entity* obj) {
    objects.push_back(obj);
}

void EntitySet::deleteObject(unsigned int index) {
    delete objects[index];
    removeObject(index);
}

void EntitySet::removeObject(unsigned int index) {
    objects.erase(objects.begin() + index);
}

int EntitySet::size() {
    return objects.size();
}


std::vector<Entity*>::iterator EntitySet::begin() {
    return objects.begin();
}

std::vector<Entity*>::iterator EntitySet::end() {
    return objects.end();
}


std::vector<Entity*> EntitySet::getSortedVector() {
    std::vector<Entity*> result = objects;
    std::sort(result.begin(), result.end(), [](Entity* a, Entity* b) {
        return a->getZIndex() > b->getZIndex();
    });
    return result;
}


EntitySet EntitySet::copy() const {
    EntitySet result;

    for (auto it : this->objects) {
        Entity* obj = new Entity(*it);
        result.addObject(obj);
    }
    return result;
}


EntitySet::EntitySet(EntitySet& set) {
    this->objects = set.objects;
}

EntitySet::EntitySet() {
    this->objects = std::vector<Entity*>();
}
