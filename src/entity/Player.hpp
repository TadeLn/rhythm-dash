#pragma once


class Player;

#include "entity/SpriteEntity.hpp"



class Player : public SpriteEntity {


public:
    void draw(sf::RenderTarget* tgt);
    
    Player();


};
