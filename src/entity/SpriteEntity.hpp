#pragma once


class SpriteEntity;

#include "entity/Entity.hpp"
#include "renderer/SpriteInfo.hpp"



class SpriteEntity : public Entity {


public:
    void draw(sf::RenderTarget* target);

    SpriteEntity();
    SpriteEntity(std::string spriteId);


protected:
    void setSprite(std::string spriteId);
    void setSpriteAnimation(int spriteAnimation);

    void updateSprite();

    std::string spriteId;
    int spriteAnimation;
    SpriteInfo spriteInfo;
    sf::Sprite sprite;


};
