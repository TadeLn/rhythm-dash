#include "Player.hpp"



void Player::draw(sf::RenderTarget* target) {
    SpriteEntity::draw(target);
}



Player::Player() : SpriteEntity() {
    this->setSprite("entity/player");
    this->setSpriteAnimation(0);
}
