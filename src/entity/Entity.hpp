#pragma once


#include <SFML/Graphics.hpp>

class Entity;

#include "world/World.hpp"



class Entity {


public:
    long getUUID();

    sf::Vector2f getPos();
    void setPos(sf::Vector2f pos);
    void setPosX(float x);
    void setPosY(float y);

    sf::Vector2f getSize();
    void setSize(sf::Vector2f size);
    void setSizeX(float x);
    void setSizeY(float y);

    float getRotation();
    void setRotation(float rotation);

    int getZIndex();
    void setZIndex(int);


    virtual bool onSpawn(World* world);


    Entity();
    Entity(Entity& obj);


    virtual void draw(sf::RenderTarget* target);


    static void setNextUUID(long nextUUID);

protected:
    long UUID;
    sf::Vector2f pos; // relative to chunk
    sf::Vector2f size;
    float rotation;
    int zIndex;

private:
    static long nextUUID;


};
