#include "Entity.hpp"



long Entity::getUUID() {
    return UUID;
}


sf::Vector2f Entity::getPos() {
    return pos;
}

void Entity::setPos(sf::Vector2f pos) {
    this->pos = pos;
}

void Entity::setPosX(float x) {
    pos.x = x;
}

void Entity::setPosY(float y) {
    pos.y = y;
}


sf::Vector2f Entity::getSize() {
    return size;
}

void Entity::setSize(sf::Vector2f size) {
    this->size = size;
}

void Entity::setSizeX(float x) {
    size.x = x;
}

void Entity::setSizeY(float y) {
    size.y = y;
}


float Entity::getRotation() {
    return rotation;
}

void Entity::setRotation(float rotation) {
    this->rotation = rotation;
}


int Entity::getZIndex() {
    return zIndex;
}

void Entity::setZIndex(int zIndex) {
    this->zIndex = zIndex;
}


bool Entity::onSpawn(World* world) {
    return true;
}


Entity::Entity() {
    this->UUID = nextUUID++;
    this->pos = sf::Vector2f(0, 0);
    this->size = sf::Vector2f(32, 32);
    this->rotation = 0;
    this->zIndex = 0;
}

Entity::Entity(Entity& obj) {
    this->UUID = nextUUID++;
    this->pos = obj.pos;
    this->size = obj.size;
    this->rotation = obj.rotation;
    this->zIndex = obj.zIndex;
}



void Entity::draw(sf::RenderTarget* target) {
    sf::RectangleShape rectangle(this->size);
    rectangle.setPosition(this->pos);
    rectangle.setRotation(this->rotation);
    rectangle.setFillColor(sf::Color(0xff00ffbf));
    target->draw(rectangle);
}


long Entity::nextUUID = 12345;
