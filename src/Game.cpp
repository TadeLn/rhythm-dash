#include "Game.hpp"


#include <chrono>
#include <thread>

#include "tutils/Exception.hpp"
#include "tutils/Log.hpp"

#include "debug/Debug.hpp"
#include "input/Keyboard.hpp"
#include "font/FontLoader.hpp"
#include "screen/GameScreen.hpp"
#include "sound/SoundManager.hpp"



Level& Game::getLevel() {
    return this->level;
}

World& Game::getWorld() {
    return this->level.world;
}


int Game::start(std::vector<std::string> args) {

    tu::Log::open("game.log");
    tu::Log::log("Starting game...");

    this->running = true;

    FontLoader::loadFromFile("Monofonto", "assets/fonts/monofonto.otf");
    SoundManager::init();


    r = new Renderer(this);
    r->start();


    openScreen(new GameScreen());

    
    try {
        this->level = Level::loadFromFile("assets/levels/01.json");
        this->getWorld().init();
    } catch (tu::Exception& e) {
        e.print(EXCTX);
    }


    std::chrono::steady_clock::time_point lastLoop = std::chrono::steady_clock::now();
    while (this->isRunning()) {
        std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();

        loop(std::chrono::duration_cast<std::chrono::microseconds>(now - lastLoop).count() / 1000);
        lastLoop = now;

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    tu::Log::log("Main loop exit");

    return 0;
}

void Game::stop() {
    tu::Log::log("Stopping the game...");
    this->running = false;

    tu::Log::log("Closing log file...");
    tu::Log::close();
}

bool Game::isRunning() {
    return this->running;
}

void Game::exit() {
    // [TODO] show exit confirmation message box
    this->stop();
}


void Game::handleKey(sf::Event e) {
    Keyboard::update(e);

    this->currentScreen->handleKey(e);
}

void Game::handleMouse(sf::Event e) {
    this->currentScreen->handleMouse(e);
}

void Game::handleResize(sf::Event e) {
    sf::Vector2f winSize = sf::Vector2f(e.size.width, e.size.height);
    r->realView = sf::View(sf::Vector2f(winSize.x / 2, winSize.y / 2), winSize);

    this->currentScreen->handleResize(e);
}

void Game::handleSignal(int signum) {
    tu::Log::info("Interrupt signal (", signum, ") received");
    this->stop();
}



void Game::openScreen(Screen* screen) {
    screen->setParentScreen(this->currentScreen);
    if (screen->open(this)) {
        this->currentScreen = screen;
    }
}

void Game::replaceScreen(Screen* screen) {
    Screen* currentScreen = this->currentScreen;
    bool isClosed = currentScreen->close();

    if (isClosed) {
        this->currentScreen = currentScreen->getParentScreen();
        delete currentScreen;
        openScreen(screen);
    }
}

void Game::closeScreen() {
    Screen* currentScreen = this->currentScreen;
    bool isClosed = currentScreen->close();

    if (isClosed) {
        if (currentScreen->hasParentScreen()) {
            this->currentScreen = currentScreen->getParentScreen();
            delete currentScreen;
        } else {
            this->exit();
        }
    }
}




void Game::loop(float deltaTime) {
    this->currentScreen->loop(deltaTime);

    r->updateCamera();
}
