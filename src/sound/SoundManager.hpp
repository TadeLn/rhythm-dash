#pragma once


#include <string>
#include <map>
#include <memory>
#include <vector>

#include <SFML/Audio.hpp>


class SoundManager {


public:
    enum Channel {
        GUI_SOUND
    };

    static std::map<std::string, std::shared_ptr<sf::SoundBuffer>> loadedSoundData;
    
    static void init();

    static std::shared_ptr<sf::SoundBuffer> load(std::string filename, std::string id = "");
    static std::shared_ptr<sf::SoundBuffer> getBuffer(std::string soundId);

    static void cleanup();
    static void play(std::string filename);
    static void play(std::string filename, Channel channel);


private:
    static std::vector<std::shared_ptr<sf::Sound>> activeSounds;
    static std::map<Channel ,std::shared_ptr<sf::Sound>> activeSoundChannels;



};
