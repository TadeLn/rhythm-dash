#include "SpriteInfo.hpp"


#include "tutils/Log.hpp"
#include "tutils/json/Object.hpp"



bool SpriteInfo::loadJSON(std::shared_ptr<tu::json::Node> json) {
    try {
        auto root = json->cast<tu::json::Object>();

        auto rect = root->getProperty("rect")->cast<tu::json::Object>();
        this->rect.left = rect->getProperty("x")->getEx<int>();
        this->rect.top = rect->getProperty("y")->getEx<int>();
        this->rect.width = rect->getProperty("w")->getEx<int>();
        this->rect.height = rect->getProperty("h")->getEx<int>();

        auto origin = root->getProperty("origin");
        if (origin->getType() == tu::json::Node::OBJECT) {
            this->origin.x = origin->getProperty("x")->getEx<int>();
            this->origin.y = origin->getProperty("y")->getEx<int>();
        }

    } catch (tu::Exception& e) {
        e.print("While loading SpriteInfo");
        return false;
    }
    return true;
}
