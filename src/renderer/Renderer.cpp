#include "Renderer.hpp"


#include <iostream>
#include <math.h>

#include "tutils/Log.hpp"
#include "tutils/Pair.hpp"
#include "tutils/String.hpp"

#include "font/FontLoader.hpp"
#include "debug/Debug.hpp"
#include "TextureManager.hpp"
#include "ShaderManager.hpp"
#include "entity/Entity.hpp"



void Renderer::updateCamera() {
    if (!camera.shouldUpdateCamera()) return;
    camera.clearShouldUpdateCamera();

    sf::Vector2u winSizeI = this->win->getSize();
    sf::Vector2f winSize = sf::Vector2f(winSizeI.x, winSizeI.y);

    float windowAspectRatio = (float)winSize.x / winSize.y;
    sf::Vector2f viewSize = sf::Vector2f(this->camera.getZoom() * DEFAULT_CAMERA_RANGE, this->camera.getZoom() * DEFAULT_CAMERA_RANGE / windowAspectRatio);
    
    tu::Pair<float> centerPosition = this->camera.getPosition();
    this->gameView = sf::View(sf::Vector2f(centerPosition.x, centerPosition.y), viewSize);
    this->hudView = sf::View(sf::Vector2f(DEFAULT_CAMERA_RANGE / 2, (DEFAULT_CAMERA_RANGE / windowAspectRatio) / 2), sf::Vector2f(DEFAULT_CAMERA_RANGE, DEFAULT_CAMERA_RANGE / windowAspectRatio));

    // Log::log("Window Aspect Ratio: (", windowAspectRatio, ")");
    // Log::log("Center: (", cameraCenter.x, ", ", cameraCenter.y, ") Size: (", viewSize.x, ", ", viewSize.y, ")");
}



void Renderer::start() {
    tu::Log::log("Renderer: start");

    versionFullString = "Rhythm Dash v. dev";
    #ifdef GIT_BRANCH
        versionFullString.append(" (").append(GIT_BRANCH);
        #ifdef GIT_COMMIT_HASH
            versionFullString.append("/").append(GIT_COMMIT_HASH);
        #endif
        versionFullString.append(")");
    #endif


    auto now = std::chrono::steady_clock::now();
    this->startTimestamp = now;


    this->fpsCounter = 0;
    this->lastFps = 0;
    this->lastFpsCheck = now;
    
    debugText.setFont(*FontLoader::getDefault());
    debugText.setPosition(0, 0);
    debugText.setCharacterSize(100);
    debugText.setScale(0.15, 0.15);
    debugText.setOutlineColor(sf::Color::Black);
    debugText.setOutlineThickness(10);


    this->win = new sf::RenderWindow(sf::VideoMode(800, 600), versionFullString);
    this->win->setKeyRepeatEnabled(false);
    this->win->setVerticalSyncEnabled(true);
    this->win->setActive(false);



    TextureManager::init();
    ShaderManager::init();



    this->camera.setShouldUpdateCamera();
    this->updateCamera();


    this->thread = std::thread([this]() {
        tu::Log::log("Renderer: loop initialization");

        lastLoop = std::chrono::steady_clock::now();
        this->win->setActive(true);
        while (this->g->isRunning()) {
            std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();

            this->loop(std::chrono::duration_cast<std::chrono::microseconds>(now - lastLoop).count() / 1000);
            lastLoop = now;
            // std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    });


    tu::Log::log("Renderer: initialization done");
}



void Renderer::loop(float deltaTime) {
    auto now = this->lastLoop;

    sf::Event e;
    while (win->pollEvent(e)) {
        switch (e.type) {
            case sf::Event::Closed:
                g->stop();
                break;

            case sf::Event::Resized:
                camera.setShouldUpdateCamera();
                updateCamera();

                g->handleResize(e);
                break;

            case sf::Event::KeyPressed:
            case sf::Event::KeyReleased:
                g->handleKey(e);
                break;

            case sf::Event::MouseMoved:
            case sf::Event::MouseButtonPressed:
            case sf::Event::MouseButtonReleased:
            case sf::Event::MouseWheelScrolled:
            case sf::Event::MouseWheelMoved:
                g->handleMouse(e);
                break;
            
            default:
                break;
        }
    }

    if (!win->isOpen()) {
        g->stop();
    }


    Debug::clearString();
    if (Debug::enabled) {
        Debug::addLine(versionFullString);
    }

    if ((now - this->lastFpsCheck) > std::chrono::seconds(1)) {
        this->lastFps = this->fpsCounter;
        this->fpsCounter = 0;
        this->lastFpsCheck = now;
    }
    if (Debug::enabled) {
        Debug::addLine(Util::str("FPS: ", this->lastFps, "\n"));
    }
    


    win->clear(sf::Color::Black);


    if (g->currentScreen) {
        g->currentScreen->render(this, this->win);
    }


    // Render Debug HUD
    win->setView(this->realView);

    if (Debug::enabled) {
        debugText.setString(Debug::getString());

        win->draw(debugText);
    }

    // End render
    ++this->fpsCounter;
    win->display();
}



Renderer::Renderer(Game* gamePtr) {
    this->g = gamePtr;
    this->camera = Camera();
}



const std::chrono::steady_clock::time_point& Renderer::getStartTimestamp() {
    return this->startTimestamp;
}



void Renderer::renderWorld(World& world, sf::RenderTarget* target) {
    auto now = this->lastLoop;

    sf::View view = this->gameView;
    const int TILE_SIZE = this->TILE_SIZE;
    const int TILE_TEXTURE_SIZE = this->TILE_TEXTURE_SIZE;

    // In debug mode, the camera is more zoomed out, to debug optimized rendering
    if (Debug::cameraDebug) {
        view.setSize(sf::Vector2f(this->gameView.getSize().x * 2, this->gameView.getSize().y * 2));
    }

    // Render World
    target->setView(view);

    sf::Vector2f topLeftPixel = this->gameView.getCenter() - sf::Vector2f(this->gameView.getSize().x / 2, this->gameView.getSize().y / 2);
    sf::Vector2i topLeftTile = sf::Vector2i(
        floor(topLeftPixel.x / TILE_SIZE),
        floor(topLeftPixel.y / TILE_SIZE)
    );
    Debug::addLine(Util::str(
        "TopLeftPixel: (", topLeftPixel.x, ", ", topLeftPixel.y, ") ",
        " TopLeftTile: (", topLeftTile.x, ", ", topLeftTile.y, ")"
    ));

    sf::RectangleShape tile;
    tile.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
    tile.setTexture(&TextureManager::getTexture());

    sf::Vector2i cameraTileRange = sf::Vector2i(
        floor(this->gameView.getSize().x / TILE_SIZE) - 1,
        floor(this->gameView.getSize().y / TILE_SIZE) - 1
    );



    int tileCounter = 0;
    for (int y = topLeftTile.y; y <= topLeftTile.y + cameraTileRange.y + 2; y++) {
        for (int x = topLeftTile.x; x <= topLeftTile.x + cameraTileRange.x + 2; x++) {
            TileID tileID = world.getTile(x, y);

            if (tileID != 0) {
                SpriteInfo spr = world.paletteSprites[tileID];
                tile.setTextureRect(spr.rect);
                tile.setPosition(
                    TILE_SIZE * x,
                    TILE_SIZE * y
                );
                target->draw(tile);
                ++tileCounter;
            }
        }
    }
    if (Debug::enabled) {
        Debug::addLine(Util::str("Tiles Rendered: ", tileCounter));
    }


    // Render sprites
    auto entities = world.getEntities();

    for (auto entity : entities) {
        entity.second->draw(target);
    }


    // Render debug stuff
    if (Debug::enabled) {
        Debug::addLine(Util::str("Camera: ", this->camera));

        // Render chunk borders
        tu::Pair<int> topLeftChunk = tu::Pair<int>(
            floor((float)topLeftTile.x / CHUNK_SIZE),
            floor((float)topLeftTile.y / CHUNK_SIZE)
        );
        sf::RectangleShape chunk;
        chunk.setFillColor(sf::Color::Transparent);
        chunk.setOutlineColor(sf::Color(0xff00007f));
        chunk.setOutlineThickness(this->camera.getZoom());
        chunk.setSize(sf::Vector2f(CHUNK_SIZE * TILE_SIZE, CHUNK_SIZE * TILE_SIZE));

        int i = 
            (topLeftChunk.x + floor((float)cameraTileRange.x / CHUNK_SIZE) + 2) *
            (topLeftChunk.y + floor((float)cameraTileRange.y / CHUNK_SIZE) + 2);
            
        if (i <= 2000) {
            for (int x = topLeftChunk.x; x < topLeftChunk.x + floor((float)cameraTileRange.x / CHUNK_SIZE) + 2; x++) {
                for (int y = topLeftChunk.y; y < topLeftChunk.y + floor((float)cameraTileRange.y / CHUNK_SIZE) + 2; y++) {
                    chunk.setPosition(
                        TILE_SIZE * CHUNK_SIZE * x,
                        TILE_SIZE * CHUNK_SIZE * y
                    );
                    target->draw(chunk);
                }
            }
        }

        Debug::addLine(Util::str("TopLeftChunk: ", topLeftChunk, " | ", floor(0.7)));
        Debug::addLine(Util::str("ChunkBorders: ", i));
        Debug::addLine(Util::str("CameraTileRange: (", cameraTileRange.x, ", ", cameraTileRange.y, ")"));



        // Render debug camera range
        if (Debug::cameraDebug) {
            sf::RectangleShape cameraRange;
            cameraRange.setFillColor(sf::Color::Transparent);
            cameraRange.setOutlineColor(sf::Color::Red);
            cameraRange.setOutlineThickness(this->camera.getZoom());
            cameraRange.setSize(this->gameView.getSize());
            cameraRange.setPosition(this->gameView.getCenter() -
                sf::Vector2f(
                    cameraRange.getSize().x / 2,
                    cameraRange.getSize().y / 2
                )
            );
            target->draw(cameraRange);
        }


        // Render cursor
        {
            sf::RectangleShape cursor;
            cursor.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
            cursor.setFillColor(sf::Color(0xffffff3f));
            cursor.setOutlineColor(sf::Color(0xffffffff));
            cursor.setOutlineThickness(1);

            sf::Vector2f coords = target->mapPixelToCoords(sf::Mouse::getPosition(*this->win));
            tu::Pair<int> cursorPos = tu::Pair<int>(
                floor(coords.x / TILE_SIZE),
                floor(coords.y / TILE_SIZE)
            );

            cursor.setPosition(cursorPos.x * TILE_SIZE, cursorPos.y * TILE_SIZE);


            auto s = ShaderManager::getShader("blackhole");
            if (s != nullptr) {
                s->setUniform("time", (float)this->getTime<std::chrono::milliseconds>(now).count() / 1000.f);
            }
            // target->draw(cursor, s);
            target->draw(cursor);

            Debug::addLine(Util::str("CursorPos: ", cursorPos));
        }
    }



    float pixelSize, pixelSizeWithZoom;
    if (Debug::enabled) {
        pixelSize = (float)target->getSize().y / (float)this->hudView.getSize().y;
        pixelSizeWithZoom = (float)target->getSize().y / (float)this->gameView.getSize().y;
        Debug::addLine(Util::str("PixelSize: ", pixelSize, " WithZoom: ", pixelSizeWithZoom));
    }

    Debug::addLine(Util::str("TestString: ", g->getLevel().test));
}
