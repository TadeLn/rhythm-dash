#include "TextureManager.hpp"


#include "tutils/json/Node.hpp"
#include "tutils/json/Object.hpp"
#include "tutils/Log.hpp"

#include "SpriteInfo.hpp"



void TextureManager::init() {
    const std::string textureFilename = "assets/textures/all.png";
    if (!texture.loadFromFile(textureFilename)) {
        tu::Log::error("TextureManager: Failed to load texture from file ", textureFilename, "\n");
    }
    texture.setSmooth(false);
    

    try {
        auto j = tu::json::Node::fromFile("assets/textures/all.json");
        auto root = j->cast<tu::json::Object>();
        auto sprites = root->getPropertyEx("textures")->getEx<tu::json::Node::dictionary>();

        for (auto it : sprites) {
            SpriteInfo info;
            if (info.loadJSON(it.second)) {
                tu::Log::log("Loaded texture \"", it.first, "\"");
                TextureManager::info[it.first] = info;
            } else {
                tu::Log::error("Could not load texture \"", it.first, "\"");
            }
        }

    } catch (tu::Exception& e) {
        e.print("Texture load error");
    }
}


SpriteInfo TextureManager::getInfo(std::string key) {
    auto it = info.find(key);
    if (it != info.end()) {
        return it->second;
    } else {
        return info.at("undefined");
    }
}

SpriteInfo TextureManager::getInfo(std::string key, int animation) {
    return getInfo(key + "_" + std::to_string(animation));
}

sf::Texture& TextureManager::getTexture() {
    return texture;
}



sf::Texture TextureManager::texture;

std::map<std::string, SpriteInfo> TextureManager::info;
