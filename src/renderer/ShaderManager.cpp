#include "ShaderManager.hpp"


#include "tutils/Log.hpp"



void ShaderManager::init() {
    {
        auto s = load("fire", "assets/shaders/fire.glsl", sf::Shader::Fragment);
        if (s) {
            s->setUniform("time", 0.f);
            s->setUniform("mouse", sf::Vector2f(200, 130));
            s->setUniform("resolution", sf::Vector2f(400, 300));
        }
    }
    {
        auto s = load("blackhole", "assets/shaders/blackhole.glsl", sf::Shader::Fragment);
        if (s) {
            s->setUniform("time", 0.f);
            s->setUniform("mouse", sf::Vector2f(200, 130));
            s->setUniform("resolution", sf::Vector2f(400, 300));
        }
    }
}

sf::Shader* ShaderManager::getShader(std::string key) {
    auto it = shaders.find(key);
    if (it != shaders.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}





sf::Shader* ShaderManager::load(std::string key, std::string filename, sf::Shader::Type type) {
    tu::Log::log("Loading shader \"", key, "\" (\"", filename, "\")");

    sf::Shader* shader = new sf::Shader();
    shader->loadFromFile(filename, type);

    if (shader->isAvailable()) {
        auto it = shaders.find(key);
        if (it != shaders.end()) {
            delete it->second;
        }
        shaders[key] = shader;
        return shader;

    } else {
        tu::Log::error("The shader ", key, " is not available");
    }
    delete shader;
    return nullptr;
}

std::map<std::string, sf::Shader*> ShaderManager::shaders;
