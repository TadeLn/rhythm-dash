#pragma once


#include <SFML/Graphics.hpp>

#include "SpriteInfo.hpp"



class TextureManager {


public:
    static void init();

    static SpriteInfo getInfo(std::string key);
    static SpriteInfo getInfo(std::string key, int animation);
    static sf::Texture& getTexture();

private:
    static sf::Texture texture;
    static std::map<std::string, SpriteInfo> info;


};
