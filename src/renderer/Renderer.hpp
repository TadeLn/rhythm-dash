#pragma once


#include <thread>
#include <SFML/Graphics.hpp>


class Renderer;

#include "world/World.hpp"
#include "Game.hpp"
#include "Camera.hpp"



class Renderer {

public:
    // Horizontal camera range when zoom is = 1 (in game world pixels)
    // Vertical camera range is set automatically based on the window aspect ratio
    const int DEFAULT_CAMERA_RANGE = 500;

    const int TILE_SIZE = 32;
    const int TILE_TEXTURE_SIZE = 32;


    Camera camera;
    void updateCamera();

    void start();
    void loop(float deltaTime);
    Renderer(Game* gamePtr);


    const std::chrono::steady_clock::time_point& getStartTimestamp();

    template<typename T = std::chrono::milliseconds>
    T getTime(std::chrono::steady_clock::time_point& now) {
        return std::chrono::duration_cast<T>(now - this->startTimestamp);
    }


    void renderWorld(World& world, sf::RenderTarget* target);


    Game* g;

    sf::RenderWindow* win;

    // This view scales with the window width
    // The view's width stays constant when resizing, and the height adjusts accordingly
    // The view's position depends on the camera
    sf::View gameView;

    // This view scales with the window width
    // The view's width stays constant, and the height adjusts accordingly
    // The view's position is independend from the camera;
    sf::View hudView;

    // This view resizes with the window
    // The view's size is always equal to the window's size
    // The view's position is in the middle of the window, such that (0, 0) is always in the top left corner
    sf::View realView;

private:
    std::string versionFullString;

    std::chrono::steady_clock::time_point startTimestamp;
    std::chrono::steady_clock::time_point lastLoop;

    std::chrono::steady_clock::time_point lastFpsCheck;
    int fpsCounter;
    int lastFps;

    sf::Text debugText;

    std::thread thread;


};