#pragma once


#include <SFML/Graphics.hpp>

#include "tutils/json/Deserializable.hpp"



class SpriteInfo : public tu::json::Deserializable {


public:
    // Fragment of the whole texture
    sf::IntRect rect;

    // Where in the rect is (0, 0) of the texture
    sf::Vector2i origin;

    virtual bool loadJSON(std::shared_ptr<tu::json::Node> json);


};
