#pragma once


#include <SFML/Graphics.hpp>



class ShaderManager {


public:
    static void init();

    static sf::Shader* getShader(std::string key);

private:
    static sf::Shader* load(std::string key, std::string filename, sf::Shader::Type type);
    static std::map<std::string, sf::Shader*> shaders;


};
