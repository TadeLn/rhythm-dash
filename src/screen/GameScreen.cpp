#include "GameScreen.hpp"


#include "debug/Debug.hpp"
#include "world/Tile.hpp"
#include "input/Keyboard.hpp"
#include "TestMenuScreen.hpp"
#include "renderer/TextureManager.hpp"



bool GameScreen::open(Game* g) {
    Screen::open(g);
    return true;
}

void GameScreen::loop(float deltaTime) {
    Renderer* r = g->r;

    if (Keyboard::get(sf::Keyboard::Left)) {
        r->camera.move(-32 * deltaTime / 100, 0);
    }
    if (Keyboard::get(sf::Keyboard::Right)) {
        r->camera.move(32 * deltaTime / 100, 0);
    }
    if (Keyboard::get(sf::Keyboard::Up)) {
        r->camera.move(0, -32 * deltaTime / 100);
    }
    if (Keyboard::get(sf::Keyboard::Down)) {
        r->camera.move(0, 32 * deltaTime / 100);
    }
    if (Keyboard::get(sf::Keyboard::PageUp)) {
        r->camera.setZoom(r->camera.getZoom() * (1 + (deltaTime / 1000)));
    }
    if (Keyboard::get(sf::Keyboard::PageDown)) {
        r->camera.setZoom(r->camera.getZoom() / (1 + (deltaTime / 1000)));
    }
}



void GameScreen::render(Renderer* r, sf::RenderTarget* target) {
    target->clear(sf::Color(g->getLevel().backgroundColor));
    r->renderWorld(g->getWorld(), target);
}



bool GameScreen::handleKey(sf::Event e) {
    Renderer* r = g->r;
    
    if (e.type == sf::Event::KeyPressed) {
        if (e.key.code == sf::Keyboard::Equal) {
            r->camera.setZoom(1);
            r->camera.setPosition(0, 0);
            return true;
        }
        if (e.key.code == sf::Keyboard::F3) {
            Debug::enabled = !Debug::enabled;
            return true;
        }
        if (e.key.code == sf::Keyboard::F4) {
            Debug::cameraDebug = !Debug::cameraDebug;
            return true;
        }
        if (e.key.code == sf::Keyboard::Escape) {
            g->exit();
            return true;
        }
        if (e.key.code == sf::Keyboard::T) {
            g->openScreen(new TestMenuScreen());
            return true;
        }
    }
    return false;
}



bool GameScreen::handleMouse(sf::Event e) {
    Renderer* r = g->r;

    if (e.type == sf::Event::MouseWheelScrolled) {
        r->camera.setZoom(r->camera.getZoom() / (1 + (e.mouseWheelScroll.delta / 10)));
        return true;
    }
    return false;
}
