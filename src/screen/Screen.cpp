#include "Screen.hpp"



Screen* Screen::getParentScreen() {
    return this->parentScreen;
}

bool Screen::hasParentScreen() {
    return this->parentScreen != nullptr;
}

void Screen::setParentScreen(Screen* parentScreen) {
    this->parentScreen = parentScreen;
}


bool Screen::open(Game* g) {
    this->g = g;
    return true;
}

bool Screen::close() {
    return true;
}

void Screen::loop(float deltaTime) {

}

void Screen::render(Renderer* r, sf::RenderTarget* target) {

}

bool Screen::handleKey(sf::Event e) {
    return false;
}

bool Screen::handleMouse(sf::Event e) {
    return false;
}

bool Screen::handleResize(sf::Event e) {
    return false;
}
