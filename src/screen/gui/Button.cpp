#include "Button.hpp"


#include "tutils/Log.hpp"

#include "sound/SoundManager.hpp"



bool Button::click(tu::Pair<int> mousePos) {
    if (clickCallback) {
        if (clickCallback(mousePos)) {
            SoundManager::play("gui/button_confirm", SoundManager::GUI_SOUND);
            return true;
        }
    }
    return false;
}

void Button::hover() {
    BasicWidget::hover();
    SoundManager::play("gui/button_hover", SoundManager::GUI_SOUND);

    this->rectangle.setFillColor(HOVER_COLOR);
}

void Button::unhover() {
    BasicWidget::unhover();
    this->rectangle.setFillColor(NORMAL_COLOR);
}

void Button::parentResize(tu::Pair<int> parentSize) {
    if (parentResizeCallback) {
        parentResizeCallback(parentSize);
    }
}

void Button::render(tu::Pair<int> parentOffset, sf::RenderTarget* target) {
    auto size = this->getSize();
    auto position = this->getPos() + parentOffset;
    
    rectangle.setSize(sf::Vector2f(size.x, size.y));
    rectangle.setPosition(position.x, position.y);

    target->draw(rectangle);
}



Button::Button(
    tu::Pair<int> pos,
    tu::Pair<int> size,
    bool (*clickCallback)(tu::Pair<int> mousePos),
    void (*parentResizeCallback)(tu::Pair<int> parentSize)
) {
    this->pos = pos;
    this->size = size;
    this->clickCallback = clickCallback;
    this->parentResizeCallback = parentResizeCallback;

    rectangle = sf::RectangleShape();
    rectangle.setOutlineColor(sf::Color::Black);
    rectangle.setOutlineThickness(2);
    rectangle.setFillColor(NORMAL_COLOR);
}
