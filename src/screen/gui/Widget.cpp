#include "Widget.hpp"



bool Widget::isInRect(tu::Pair<int> coords) {
    auto pos = getPos();
    auto size = getSize();
    if (pos.x <= coords.x && coords.x < pos.x + size.x) {
        if (pos.y <= coords.y && coords.y < pos.y + size.y) {
            return true;
        }
    }
    return false;
}


bool Widget::click(tu::Pair<int> mousePos) {
    return false;
}

void Widget::mouseMove(tu::Pair<int> mousePos) {

}

void Widget::hover() {

}

void Widget::unhover() {

}

void Widget::parentResize(tu::Pair<int> parentSize) {

}


void Widget::render(tu::Pair<int> parentOffset, sf::RenderTarget* target) {

}
