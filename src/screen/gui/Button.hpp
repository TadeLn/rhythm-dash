#pragma once


#include "BasicWidget.hpp"



class Button : public BasicWidget {


public:
    const sf::Color NORMAL_COLOR = sf::Color(0x222222ff);
    const sf::Color HOVER_COLOR = sf::Color(0x772277ff);

    virtual bool click(tu::Pair<int> mousePos);
    virtual void hover();
    virtual void unhover();
    virtual void parentResize(tu::Pair<int> parentSize);
    virtual void render(tu::Pair<int> parentOffset, sf::RenderTarget* target);

    Button(
        tu::Pair<int> pos,
        tu::Pair<int> size,
        bool (*clickCallback)(tu::Pair<int> mousePos) = nullptr,
        void (*parentResizeCallback)(tu::Pair<int> parentSize) = nullptr
    );


protected:
    bool (*clickCallback)(tu::Pair<int> mousePos);
    void (*parentResizeCallback)(tu::Pair<int> parentSize);
    sf::RectangleShape rectangle;


};
