#pragma once


#include <SFML/Graphics.hpp>

#include "tutils/Pair.hpp"



class Widget {


public:
    // Returns x and y position of the widget relative to the parent
    virtual tu::Pair<int> getPos() = 0;

    // Returns width and height of the widget
    virtual tu::Pair<int> getSize() = 0;

    // Returns true if coords are inside the widget rect
    bool isInRect(tu::Pair<int> coords);


    // Triggered on mouse click
    // mousePos is relative to the widget
    // Returns true if the event has been handled
    virtual bool click(tu::Pair<int> mousePos);
    
    // Triggered on mouse move
    // mousePos is relative to the widget
    virtual void mouseMove(tu::Pair<int> mousePos);

    // Triggered on mouse hover
    virtual void hover();

    // Triggered on mouse unhover
    virtual void unhover();

    // Returns if the widget is hovered or not
    virtual bool isHovered() = 0;

    // Triggered when parent widget is resized
    virtual void parentResize(tu::Pair<int> parentSize);


    // Render widget onto target
    virtual void render(tu::Pair<int> parentOffset, sf::RenderTarget* target);


};
