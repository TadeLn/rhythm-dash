#pragma once


#include <vector>
#include <memory>

#include "BasicWidget.hpp"



class Container : public BasicWidget {


public:
    virtual void setSize(tu::Pair<int> size);

    virtual bool click(tu::Pair<int> mousePos);
    virtual void mouseMove(tu::Pair<int> mousePos);
    virtual void unhover();
    virtual void parentResize(tu::Pair<int> parentSize);
    virtual void render(tu::Pair<int> parentOffset, sf::RenderTarget* target);


    std::vector<std::shared_ptr<Widget>>& getChildren();


protected:
    std::vector<std::shared_ptr<Widget>> widgets;


};
