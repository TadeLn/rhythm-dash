#include "BasicWidget.hpp"



tu::Pair<int> BasicWidget::getPos() {
    return this->pos;
}

tu::Pair<int> BasicWidget::getSize() {
    return this->size;
}


void BasicWidget::setPos(tu::Pair<int> pos) {
    this->pos = pos;
}

void BasicWidget::setSize(tu::Pair<int> size) {
    this->size = size;
}


void BasicWidget::hover() {
    this->hovered = true;
}

void BasicWidget::unhover() {
    this->hovered = false;
}

bool BasicWidget::isHovered() {
    return this->hovered;
}

