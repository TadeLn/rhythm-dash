#pragma once


#include "Widget.hpp"



class BasicWidget : public Widget {


public:
    virtual tu::Pair<int> getPos();
    virtual tu::Pair<int> getSize();

    virtual void setPos(tu::Pair<int> pos);
    virtual void setSize(tu::Pair<int> size);


    virtual void hover();
    virtual void unhover();
    virtual bool isHovered();


protected:
    tu::Pair<int> pos;
    tu::Pair<int> size;
    bool hovered = false;
    


};
