#include "Container.hpp"



void Container::setSize(tu::Pair<int> size) {
    BasicWidget::setSize(size);

    for (auto it : this->widgets) {
        it->parentResize(size);
    }
}


bool Container::click(tu::Pair<int> mousePos) {
    for (auto it : this->widgets) {
        if (it->isInRect(mousePos)) {
            if (it->click(mousePos - it->getPos())) {
                return true;
            }
        }
    }
    return false;
}

void Container::mouseMove(tu::Pair<int> mousePos) {
    for (auto it : this->widgets) {
        auto relMousePos = mousePos - it->getPos();

        if (it->isInRect(mousePos)) {
            if (!it->isHovered()) {
                it->hover();
            }
            it->mouseMove(relMousePos);
        } else {
            if (it->isHovered()) {
                it->unhover();
            }
        }
    }
}

void Container::unhover() {
    for (auto it : this->widgets) {
        if (it->isHovered()) {
            it->unhover();
        }
    }
}

void Container::parentResize(tu::Pair<int> parentSize) {

}

void Container::render(tu::Pair<int> parentOffset, sf::RenderTarget* target) {
    auto newParentOffset = parentOffset + this->getPos();
    for (auto it : this->widgets) {
        it->render(newParentOffset, target);
    }
}



std::vector<std::shared_ptr<Widget>>& Container::getChildren() {
    return this->widgets;
}
