#pragma once


#include "GUIScreen.hpp"



class TestMenuScreen : public GUIScreen {


public:
    bool open(Game* g);

    void loop(float deltaTime);
    void render(Renderer* r, sf::RenderTarget* target);
    bool handleKey(sf::Event e);


};
