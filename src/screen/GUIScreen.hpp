#pragma once


#include <SFML/Graphics.hpp>

#include "Screen.hpp"
#include "gui/Container.hpp"



class GUIScreen : public Screen {


public:
    virtual bool handleKey(sf::Event e);
    virtual bool handleMouse(sf::Event e);

protected:
    Container root;
    sf::View& getView(Renderer* r);

    void renderGUI(Renderer* r, sf::RenderTarget* target);


};
