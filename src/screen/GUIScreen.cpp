#include "GUIScreen.hpp"



bool GUIScreen::handleKey(sf::Event e) {
    // [TODO] tab selecting textboxes / buttons
    if (e.type == sf::Event::KeyPressed) {
        
    }
    return false;
}

bool GUIScreen::handleMouse(sf::Event e) {
    if (e.type == sf::Event::MouseMoved || e.type == sf::Event::MouseButtonPressed) {
        sf::Vector2i eventPos;
        if (e.type == sf::Event::MouseMoved) {
            eventPos = sf::Vector2i(e.mouseMove.x, e.mouseMove.y);
        } else if (e.type == sf::Event::MouseButtonPressed) {
            eventPos = sf::Vector2i(e.mouseButton.x, e.mouseButton.y);
        }

        sf::View& newView = this->getView(g->r);
        auto point = g->r->win->mapPixelToCoords(eventPos, newView);

        if (e.type == sf::Event::MouseMoved) {
            this->root.mouseMove(tu::Pair<int>(point.x, point.y));
        } else if (e.type == sf::Event::MouseButtonPressed) {
            this->root.click(tu::Pair<int>(point.x, point.y));
        }

    } else if (e.type == sf::Event::MouseLeft) {
        this->root.unhover();

    } else if (e.type == sf::Event::MouseEntered) {
        this->root.hover();

    } else if (e.type == sf::Event::MouseWheelScrolled) {
        // [TODO] Move scrollable windows

    }
    return false;
}


sf::View& GUIScreen::getView(Renderer* r) {
    return r->hudView;
}

void GUIScreen::renderGUI(Renderer* r, sf::RenderTarget* target) {
    auto oldView = target->getView();
    
    target->setView(getView(r));
    this->root.render(tu::Pair<int>(0), target);

    target->setView(oldView);
}
