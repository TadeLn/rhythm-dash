#pragma once


#include <SFML/Graphics.hpp>

class Screen;

#include "Game.hpp"
#include "renderer/Renderer.hpp"


class Screen {


public:
    Screen* getParentScreen();
    bool hasParentScreen();
    void setParentScreen(Screen* parentScreen);

    // Called when the screen is opened
    // Returns true if the screen was opened successfully
    virtual bool open(Game* g);

    // Called when the screen is closed
    // Returns true if the screen was closed successfully
    virtual bool close();

    // Called on every loop cycle
    virtual void loop(float deltaTime);

    // Called on render
    virtual void render(Renderer* r, sf::RenderTarget* target);

    // Called on key event
    // Returns true if the event was handled
    virtual bool handleKey(sf::Event e);

    // Called on mouse event
    // Returns true if the event was handled
    virtual bool handleMouse(sf::Event e);

    // Called on window resize event
    // Returns true if the event was handled
    virtual bool handleResize(sf::Event e);


protected:
    Game* g = nullptr;


private:
    Screen* parentScreen = nullptr;


};
