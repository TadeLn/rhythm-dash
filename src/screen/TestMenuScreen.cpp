#include "TestMenuScreen.hpp"


#include "tutils/Log.hpp"

#include "gui/Button.hpp"



bool TestMenuScreen::open(Game* g) {
    Screen::open(g);
    this->root.getChildren().push_back(std::make_shared<Button>(
        tu::Pair<int>(20, 20),
        tu::Pair<int>(300, 40),
        [](tu::Pair<int> mousePos) {
            tu::Log::info("Button pressed");
            return true;
        }
    ));
    return true;
}

void TestMenuScreen::loop(float deltaTime) {
    
}

void TestMenuScreen::render(Renderer* r, sf::RenderTarget* target) {
    this->renderGUI(r, target);
}

bool TestMenuScreen::handleKey(sf::Event e) {
    if (e.type == sf::Event::KeyPressed) {
        if (e.key.code == sf::Keyboard::Escape) {
            g->closeScreen();
            return true;
        }
    }
    return false;
}
