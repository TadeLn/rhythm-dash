#pragma once


#include "Screen.hpp"



class GameScreen : public Screen {


public:
    bool open(Game* g);
    void loop(float deltaTime);
    void render(Renderer* r, sf::RenderTarget* target);
    virtual bool handleKey(sf::Event e);
    virtual bool handleMouse(sf::Event e);


};
