#pragma once



template<typename T>
class Coords {


public:
    T x;
    T y;

    Coords<T> operator+(Coords<T> vec) {
        return Coords<T>(this->x + vec.x, this->y + vec.y);
    }
    Coords<T> operator-(Coords<T> vec) {
        return Coords<T>(this->x - vec.x, this->y - vec.y);
    }
    Coords<T> operator*(Coords<T> vec) {
        return Coords<T>(this->x * vec.x, this->y * vec.y);
    }
    Coords<T> operator/(Coords<T> vec) {
        return Coords<T>(this->x / vec.x, this->y / vec.y);
    }

    void operator+=(Coords<T> vec) {
        this->x += vec.x;
        this->y += vec.y;
    }
    void operator-=(Coords<T> vec) {
        this->x -= vec.x;
        this->y -= vec.y;
    }
    void operator*=(Coords<T> vec) {
        this->x *= vec.x;
        this->y *= vec.y;
    }
    void operator/=(Coords<T> vec) {
        this->x /= vec.x;
        this->y /= vec.y;
    }

    Coords() {
        this->x = 0;
        this->y = 0;
    }

    Coords(T values) {
        this->x = values;
        this->y = values;
    }

    Coords(T x, T y) {
        this->x = x;
        this->y = y;
    }


    template<typename U>
    Coords(Coords<U> vec) {
        this->x = (T)vec.x;
        this->y = (T)vec.y;
    }

    template<typename U>
    operator Coords<U>() {
        return Coords<U>(&this);
    }


};
